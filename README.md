

# LE PROGRAMME DOCKER - ADMINISTRATION AVANCEE

## DOCKER ENGINE

-   Fonctionnalités, installation et configuration

	- Docker introduction : [https://docs.docker.com/engine/docker-overview/](https://docs.docker.com/engine/docker-overview/) durée 10 min

	- Docker Engine Community : [https://docs.docker.com/install/](https://docs.docker.com/install/) durée 7 min

	- Docker Engine Enterprise : [https://docs.docker.com/ee/supported-platforms/](https://docs.docker.com/ee/supported-platforms/) durée 4 min

## LE SERVICE DOCKER

- Docker daemon : rôle, configuration des principales options
[https://docs.docker.com/config/daemon/](https://docs.docker.com/config/daemon/) durée 11 min

- Option socket pour les accès en réseau
[https://docs.docker.com/v17.09/engine/userguide/networking/#default-networks](https://docs.docker.com/v17.09/engine/userguide/networking/#default-networks) durée 20 min

- Variables d'environnement : DOCKER_HOST, et DOCKER_TLS_VERIFY

	- Protection du daemon : [https://docs.docker.com/engine/security/https/](https://docs.docker.com/engine/security/https/) durée 7 min

	- Vérification des variables d'environnement : [https://docs.docker.com/machine/reference/env/](https://docs.docker.com/machine/reference/env/) durée 3 min

- Option storage-driver : définition des formats de stockage des images
[https://docs.docker.com/storage/storagedriver/](https://docs.docker.com/storage/storagedriver/) durée 15 min

- Gestion de noeuds avec l'option cluster-advertise
[https://docs.docker.com/network/overlay-standalone.swarm/](https://docs.docker.com/network/overlay-standalone.swarm/) durée 12 min

- Configuration des accès réseau et de clusters Docker
[https://docs.docker.com/swarm/networking/](https://docs.docker.com/swarm/networking/) durée 3 min

## Bonus :
- toutes les commandes Docker
[https://docs.docker.com/engine/reference/commandline/dockerd/](https://docs.docker.com/engine/reference/commandline/dockerd/) durée 54 min


## CRÉATION D'UN REGISTRY PRIVÉ

- Présentation de Docker Trusted Registry (DTR) 
[https://docs.docker.com/ee/dtr/](https://docs.docker.com/ee/dtr/) durée 2 min

- Architecture 
[https://docs.docker.com/ee/dtr/architecture/](https://docs.docker.com/ee/dtr/architecture/) durée 3 min

- Containers et volumes propres au DTR 
[https://docs.docker.com/ee/dtr/architecture/](https://docs.docker.com/ee/dtr/architecture/) durée 3 min

- Pilotage par UCP (Universal Control Plane) 

	- [https://docs.docker.com/ee/ucp/](https://docs.docker.com/ee/ucp/) durée 2 min

	- [https://docs.docker.com/ee/ucp/admin/install/](https://docs.docker.com/ee/ucp/admin/install/) durée 5 min


- Installation d'un dépôt privé 
[https://docs.docker.com/ee/dtr/admin/install/](https://docs.docker.com/ee/dtr/admin/install/) durée 5 min

- Gestion des images du DTR, des droits d'accès 
[https://docs.docker.com/ee/ucp/admin/configure/manage-and-deploy-private-images/](https://docs.docker.com/ee/ucp/admin/configure/manage-and-deploy-private-images/) durée 3 min


## ADMINISTRATION EN PRODUCTION

-   Applications multi-containers avec Compose : définition de l'environnement applicatif, déclaration des services dans docker-compose.yml, exécution avec docker-compose
```
	- A voir dans les Labs en pratique
```

-   Méthodes d'administration de containers en production
```
	- A voir dans les Labs en pratique
```

-   Orchestration avec Docker Machine
```
	- A voir dans les Labs en pratique
```

-   Exemples de provisionning en environnement mixte, dans le cloud et sur des machines physiques
```
	- A voir dans les Labs en pratique
```

-   Présentation de Swarm pour le clustering : fonctionnalités, gestion de clusters docker, équilibrage de charge, répartition de tâches, gestion de services répartis,.
```
	- A voir dans les Labs en pratique
```

## SÉCURITÉ

-   Analyse des points à risques : le noyau, le service Docker, les containers,...

	- [https://docs.docker.com/engine/security/security/](https://docs.docker.com/engine/security/security/) durée 11 min



-   Types de dangers : déni de service, accès réseau non autorisés, ...

	- [https://docs.docker.com/engine/security/security/](https://docs.docker.com/engine/security/security/) durée 11 min



-   Mécanismes de protection : pile réseau propre à chaque container, limitations de ressources par les cgroups, restrictions des droits d'accès sur les sockets, politique de sécurité des containers

	- [https://docs.docker.com/engine/security/security/](https://docs.docker.com/engine/security/security/) durée 11 min



-   Mise en évidence de failles de sécurité et des bonnes pratiques à adopter

	- Audit sécurité: [https://github.com/docker/docker-bench-security](https://github.com/docker/docker-bench-security)

	- Bonnes pratiques : [https://github.com/It4lik/markdownResources/tree/master/dockerSecurity](https://github.com/It4lik/markdownResources/tree/master/dockerSecurity)


-   Sécurisation des clients par des certificats
```
	- A voir dans les Labs en pratique
```

-   Principe, et mise en oeuvre avec openssl
```
	- A voir dans les Labs en pratique
```

-   Fiabilité des images déployées dans Docker : présentation de Content Trust pour signer les images
	- [https://docs.docker.com/engine/security/security/](https://docs.docker.com/engine/security/security/) durée 11 min



-   Exercices pratiques : activation de Content Trust, variable d'environnement DOCKER_CONTENT_TRUST
```
	- A voir dans les Labs en pratique
```

-   Création et déploiement d'images signées
```
	- A voir dans les Labs en pratique
```

-   Configuration réseau, sécurité et TLS
```
	- A voir dans les Labs en pratique
```


## Versions

**Dernière version stable :** 0.1

**Dernière version :** 0.1

Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/ansible.git)

## Auteurs

*  **Eric Legrand**  _alias_  [@ericlegrand](https://framagit.org/ericlegrandformation/ansible.git)

avec le soutien de Messieurs Hugh Norris et Elie Gavoty.

```
